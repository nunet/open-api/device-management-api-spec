const myStructData = {
  GPU: {
    name: "name",
    total_vram: 500,
    free_vram: 300
  },

  Resource: [500, 500, 8],

  Available: [500, 500],

  Reserved: [500, 500],

  Params : ["image_id", "model_url", ["packages"], "machine_type"], 

  Constraints : ["complexity", 500, 500, 500, 120, 12739], 

  StatusMessage: ["transaction type", "transaction status"],

  AddressNewRequest: {
    wallet: "type of wallet (cardano or etherium)" 
  },

  AddressNewResponse: {
    address: "address of wallet",
    mnemonic: "mnemonic"
  },

  MetadataRequest: {},

  MetadataResponse: {
    name: "host name",
    updated_timestamp: 0,
    resource: [500, 500, 8],
    available: [500, 500],
    reserved: [500, 500],
    network: "nunet-test",
    public_key: "public_key",
    node_id: "node_id",
    allow_cardano: true,
    gpu_info: [{
      name: "name",
      total_vram: 500,
      free_vram: 300
    }]
  },

  OnboardRequest: {
    memory: 4000,
    cpu: 10000,
    channel: 'nunet-test',
    address: 'wallet address'
  },

  OnboardResponse: {
    name: "host name",
    updated_timestamp: 0,
    resource: [500, 500, 8],
    available: [500, 500],
    reserved: [500, 500],
    network: "nunet-test",
    public_key: "public_key",
    node_id: "node_id",
    allow_cardano: true,
    gpu_info: [{
      name: "name",
      total_vram: 500,
      free_vram: 300
    }]
  },

  ProvisionedRequest : {},

  ProvisionedResponse: {
    cpu: 500,
    memory: 500,
    total_cores: 8
  },

  PeerListRequest : {},

  PeerListResponse : {
    peers: "peers info"
  },
  
  PeerSelfRequest : {},

  PeerSelfResponse : {
    info: "self peer info"
  },

  PeersChatRequest: {},

  PeersChatResponse: {},
  
  PeersChatClearRequest: {},

  PeersChatClearResponse: {},
  
  PeersChatJoinRequest: {
    stream_number: 0
  },

  PeersChatJoinResponse: {},
  
  PeersChatStartRequest: {
    peer_id: "peer-id-of-compute-host"
  },

  PeersChatStartResponse: {},
  
  PeersWSRequest: {
    peer_id: "peer-id-of-compute-host"
  },
  
  PeersWSResponse: {},
  
  RunRequestServiceRequest: {
    requester_wallet_address: "requester_wallet_address",
    max_ntx: 20,
    blockchain: "cardano",
    service_type: "service_type",
    params: ["image_id", "model_url", ["packages"], "machine_type"],
    constraints: ["complexity", 500, 500, 500, 120, 12739]
  },
  
  RunRequestServiceResponse: {
    response: "response of request service"
  },

  RunDeployRequest: {},

  RunDeployResponse: {
    response: "response of run deploy"
  },
  
  RunRequestRewardRequest: {},

  RunRequestRewardResponse: {},

  RunSendStatusRequest: {
    action: "action",
    message: ["transaction type", "transaction statusS"]
  },

  RunSendStatusReponse: {
    response: "response of send-status"
  },
  
  TelemetryFreeRequest: {},

  TelemetryFreeResponse: {
    id: 1,
    tot_cpu_hz: 500,
    price_cpu: 0.0,
    ram: 500,
    price_ram: 0.0,
    vcpu: 500,
    disk: 0.0,
    price_disk: 0.0
  },
  
  VMStartDefaultRequest: {
      kernel_image_path: 'path of kernel image',
      filesystem_path: 'path of filesystem'
  },

  VMStartDefaultResponse: {},
  
  VMStartCustomRequest: {},

  VMStartCustomResponse: {},
};
  
export default myStructData;