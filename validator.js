import { promises as fs } from 'fs';
import { parse as parseSchema } from 'ipld-schema';
import { create as createValidator } from 'ipld-schema-validator';
import myStructData from './schema-obj.js';

async function validateData() {
  try {

    // Read the IPLD schema file
    const schemaFilePath = 'device-management-api.ipldsch';
    const schemaText = await fs.readFile(schemaFilePath, 'utf8');

    // Parse the IPLD schema
    const schemaDescriptor = parseSchema(schemaText);

    // Iterate over the struct types defined in the schema
    for (const typeName in schemaDescriptor.types) {
      // Create a validator function for each struct type
      const validator = createValidator(schemaDescriptor, typeName);
      console.log('typeName: ', typeName)

      // An object that matches the schema
      const obj = myStructData[typeName]

      // Validate the object data
      const isValid = validator(obj);

      if (isValid) {
        console.log(`Object data for type ${typeName} is valid!\n`);
      } else {
        console.log(`Object data for type ${typeName} is invalid\n`);
      }
    }
  } catch (error) {
    console.error('An error occurred:', error);
  }
}

validateData();
